(() => {
    const myName = 'Hoang Van Nham'
    const _ids = []
    const uniqueID = () => {
        let id  = `id-${new Date().getTime()}`
        do {
            id = `${id}_${_ids.length}`
        } while (!_ids.includes(id))
        _ids.push(id)

        return id
    }

    const avatarBlockClass = uniqueID()
    const assigneeNameClass = uniqueID()
    const merchizeTaskClass = 'merchize-task'
    const CUSTOM_CSS = `
        .${avatarBlockClass} {
            text-align: text-center;
        }
        .${avatarBlockClass} .${assigneeNameClass} {
            text-align: text-center;
            font-size: 7px;
        }
    `


    function _getURLParameters() {
        return Array.from(new URLSearchParams(window.location.search)).reduce((o, i) => ({ ...o, [i[0]]: i[1] }), {})
    }

    function _FACActiveSprint() {
        if (!document.querySelector('#subnav-title .subnavigator-title')) {
            return
        }

        function taskItem(el) {
            var avatarImg = el.querySelector('.ghx-avatar img')

            if (!avatarImg) {
                return
            }

            var assigneeName = avatarImg.getAttribute('alt').replace('Assignee: ', '')
            if (!assigneeName) {
                return
            }

            var avatarBlock = avatarImg.parentElement
            avatarBlock.classList.add(avatarBlockClass)
            $(avatarBlock).append(`<span class="${assigneeNameClass}">${assigneeName}</span>`)


            el.classList.add(merchizeTaskClass)
        }

        const selector = `.ghx-column > [data-issue-key]:not(.${merchizeTaskClass})`
        Array.from(document.querySelectorAll(selector)).forEach(taskItem)
    }

    function _FAC() {
        const params = _getURLParameters()
        if (params.projectKey !== 'FAC') {
            return
        }

        _FACActiveSprint()
    }

    function _init() {
        if (document.location.host !== 'jira.mymerchize.com') {
            return
        }
        $(document).ready(function () {
            console.info('Customize Jira');
            $('head').append(`<styleC>${CUSTOM_CSS}</styleC>`)
            _FAC()
            setInterval(_FAC, 1000)
        })
    }

    _init()
})()
