$(document).ready(function () {
    if (['219.117.232.211', 'git.ntq.solutions'].indexOf(location.hostname) !== -1) {
        setTimeout(gitlab, 2000)
    }

    if (location.hostname === 'github.com') {
        setTimeout(github, 2000)
    }

    const cssPath = '/css/git.css';
    const classRoot = 'git-auto-review';
    const classReviewWarning = 'git-auto-review-waring';
    const classReviewLineActive = 'git-auto-review-line-active';
    const classReviewBtnLink = 'git-auto-review-link';
    const flag = '⚑';

    function uuidv4() {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            var r = Math.random() * 16 | 0, v = c === 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
    }

    function getURL(fileName, version) {
        try {
            let filePath = chrome.extension.getURL(fileName);
            if (version) {
                filePath = `${filePath}?v=${version}`;
            }

            return filePath;
        } catch(e) {
            return fileName;
        }
    }

    $.get(getURL(cssPath, new Date()), function (response) {
        $('head').append(`<style id="${uuidv4()}">${response}</style>`);
    });

    function github() {
        const regex = /\/pull\/([0-9]+)\/files/;

        if (regex.test(location.href)) {
            autoReview('.code-review');
        }
    }

    /**
     * Gilab
     */
    function gitlab() {
        const regex = /\/merge_requests\/([0-9]+)\/diffs/;

        if (regex.test(location.href)) {
            autoReview('.line_content.new');
        }
    }

    function autoReview(newLine) {
        const listCheck = [
            'console.', 'var_dump', 'print', 'die', 'exit', 'todo', 'fixme',
        ];
        const f = document.querySelector('#files')
        if (f) {
            f.setAttribute('style', 'width: auto !important; height: auto !important;')
        }
        const data = [];
        $(newLine).each(function () {
            const self = $(this);
            const text = self.text().replace(flag, '');
            listCheck.forEach(function (item) {
                if (text.toUpperCase().indexOf(item.toUpperCase()) !== -1 && self.hasClass('new')) {
                    var id = uuidv4();
                    if (self.attr('id')) {
                        id = self.attr('id');
                    }
                    self.attr('id', id);
                    if (!self.find(`.${classReviewWarning}`).length) {
                        self.find('.line').addClass('is_relative').append(`<span class="${classReviewWarning}">${flag}</span>`);
                    }

                    data.push({
                        title: text.trim(),
                        id: id,
                        type: item,
                    });
                }
            });
        });

        if ($(`body .${classRoot}`).length === 0) {
            $('body')
                .append(`<div class="${classRoot}"><h3 class="git-auto-review-header">AutoReview</h3><ul class="git-auto-review-list"></ul></div>`)
                .on('click', '.' + classReviewBtnLink, function () {
                    const self = $(this);
                    const target = $(self.attr('href'));

                    target.addClass(classReviewLineActive);
                    setTimeout(() => {
                        target.removeClass(classReviewLineActive)
                    }, 2000);
                });

            $('.git-auto-review-header').click(function () {
                $('.git-auto-review-list').finish().toggle('slide');
            });
        }

        if (data.length !== $('.' + classReviewBtnLink).length) {
            let html = '';
            data.forEach(function (item) {
                const escaped = $('<div>').text(item.title).html();
                html += `<li class="${item.type}"><a class="${classReviewBtnLink}" href="#${item.id}"><strong>${item.type}</strong>${escaped}</a></li>`;
            });
            $('body .git-auto-review-list').html(html);
        }
    }
});
